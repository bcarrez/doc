+++
title =  "Portal update: creation of projects that do not use Jenkins"
date = "2020-11-23T16:03:49+01:00"
tags = []
featured_image = ""
description = ""
+++

The maintenance on ci.inria.fr scheduled on Tuesday, September 22nd
was successful and the CI web portal is now fully working again. This
maintenance work enabled us to introduce new functionalities to the
portal, some of them are visible to anyone, and others will allow the
CI administrators to make the platform more robust.

All Inria users can now choose, on the creation of a new project,
whether the project will be associated to a Jenkins instance or not.
Projects without Jenkins instances are useful to create virtual
machines on Cloudstack to be used by other CI software such as Gitlab
CI. These projects will not have to worry about Jenkins maintenance
anymore, and the resource consumption will be reduced.

Therefore, do not hesitate to create new projects without Jenkins or
to stop unused Jenkins instances!  It will help us to reduce memory
consumption.

Concerning more internal changes, the back-end is now driven by
ansible instead of puppet: this change will make the portal easier to
maintain and evolve. Moreover, CI administrators can now have finer
control on the memory consumption of Jenkins instances: they can set
in particular the allowed amount of RAM dedicated to each instance on
demand.  Besides, they can now balance the workload among the servers
that run the Jenkins instances by moving an instance from one server
to another if necessary.

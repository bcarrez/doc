---
title: "Maintenance"
---

# Maintenance on the CI web portal on tuesday, september 22th 2020

A maintenance on the CI web portal is scheduled for next Tuesday (september 22th)

The web portal (https://ci.inria.fr/) will be unavailable during this maintenance. User operations done by the portal will be affected:

- projects creation / deletion
- users lists modification
- slaves creation (using the portal)

So please don't plan to perform this kind of operations during this day. You can do them earlier, or postpone them to wednesday.

Jenkins servers, gitlab-ci and cloudstack VMs will not be affected by this maintenance.

The maintenance is likely to take the whole day, although we will do our best to finish it as soon as possible.

Thanks for your understanding,

the CI team

# OS X Virtual machines maintenance on monday, january 8th 2020

On January, 8th a maintenance will occur to upgrade existing OS X virtual machines. This operation aims to modify their material profile.
This change is mandatory to allow the deployment of patch to support last versions of OS X.

Indeed, the Inria CI team developed a patch to CloudStack to support the very latest versions of Mac OS X.
The patch will be deployed at the same time than the scheduled maintenance.
Official Mac OS X 10.14 and 10.15 templates will be available a few weeks after the maintenance.

Also, the virtualization software we use is dropping the support for old OS X versions ( < 10.13).
We are studying various options. One of the option is to upgrade existing VMs.
If this option is a problem for you, please let us now by sending an email to ci-support@inria.fr.

During the maintenance, your OS X virtual machines may be unavailable a few minutes (will be rebooted).
To avoid problems, we encourage you to prevent builds on OS X slave during the maintenance (mark it as offline in Jenkins).

#  Web portal migration on monday, april 29th 2019



Inria continuous integration service is being modernized.
Web portal will be migrated to a new server, with an up-to-date operating system, libraries and framework used will also be updated.
Infrastructure scripts have been refactored to ease the maintenance and the evolution.
The deployment of the portal has also changed to ease automated deployments (old deployment tool is also unmaintained since months).
You will also find bug fixes' and enhancements such as the display of project quota usage (build farm).
Thanks to all contributors (DSI-SEISM, DSI-SESI, SED).

To achieve this, a maintenance of the CI portal is planned on Monday, April 29th starting at 2pm.
It should take up to 2 hours. During this time, Jenkins servers and the build farm (CloudStack) are still available.

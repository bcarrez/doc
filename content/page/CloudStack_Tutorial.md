---
title: "CloudStack Tutorial"
---
CloudStack is the cloud middleware used by the Inria Continuous
Integration service. It permits to create and administrate virtual
machines used as Jenkins slaves (i.e., virtual machines for automated
builds). This page describes the various steps needed to create slaves
and slave templates with CloudStack. Advanced users should refer to the
[CloudStack
manuals](http://docs.cloudstack.apache.org/projects/cloudstack-administration/en/4.4/virtual_machines.html).

Accessing the CloudStack platform
---------------------------------

Just click [here](https://ci-cloud.inria.fr) to connect to the
CloudStack platform.

To identify on the CloudStack platform, you should use the email and the
password used on the continuous integration portal as the login/password
pair. **The CloudStack domain is ci/*project-name* where *project-name*
is the unix name (aka the short name) of your project on the continuous
integration platform.**

Once connected to the CloudStack platform, you get access to a control
panel to administrate the virtual machines of your project.

1.  ![CloudStack Login
    Page](/doc/img/cloudstack_connexion.png "fig:CloudStack Login Page")
2.  ![CloudStack dashboard showing the number of existing virtual
    machines](/doc/img/tableau_de_bord.png "fig:CloudStack dashboard showing the number of existing virtual machines")

Creating a virtual machine from a template
------------------------------------------

**!!! A project is limited in terms of RAM, CPU and Disk size. Please
note also that there is a 24 hours delay when deleting a virtual
machines (enabling undo) that count in that limit !!!**

![](/doc/img/Cloudstack_instance_tab.png "/doc/img/Cloudstack_instance_tab.png")

The easiest way to create a virtual machine is to use a template. A
template is a preconfigured machine that you simply duplicate so no
operating system installation is needed. In this section, we go through
the steps needed to create a virtual machine from a template.

First, go to the Instances tab
![](/doc/img/Cloudstack_instances_button.png‎ "fig:/doc/res/Cloudstack_instances_button.png‎")
in the dashboard of the CloudStack platform and select the add instance
button
![](/doc/img/Cloudstack_add_instance_button.png "fig:/doc/img/Cloudstack_add_instance_button.png").

You should get a 6 steps dialog:

1.  ![Step 1: Choose from template or from ISO
    ](/doc/img/Cloudstack_add_instance_1.png "fig:Step 1: Choose from template or from ISO ")
2.  ![Step 2: Select your
    template](/doc/img/Cloudstack_add_instance_2.png "fig:Step 2: Select your template")
3.  ![Step 3: Select the amount of CPU and RAM you want for your
    VM](/doc/img/Cloudstack_add_instance_3.png "fig:Step 3: Select the amount of CPU and RAM you want for your VM")
4.  ![Step 4: Select extra
    disk](/doc/img/Cloudstack_add_instance_4.png "fig:Step 4: Select extra disk")
5.  ![Step 5: Affinity configuration / ignore this step
    ](/doc/img/Cloudstack_add_instance_5.png "fig:Step 5: Affinity configuration / ignore this step ")
6.  ![Step 6: Network configuration / ignore this step
    ](/doc/img/Cloudstack_add_instance_6.png "fig:Step 6: Network configuration / ignore this step ")
7.  ![Step 6: Confirm the
    configuration](/doc/img/Cloudstack_add_instance_7.png "fig:Step 6: Confirm the configuration")

To create from an existing template:

1.  select "Template"
2.  select the desired template. The template in the Community tabs are
    the one provided by the users of the continuous integration service
    and probably the ones you want to select
3.  select the size of your VM, **please select the minimum amount as
    much as possible** to avoid useless resources consumption
4.  if you need extra disk space (by default, 20Gb disks are created but
    a part of if is used by the operating system), you can select some
5.  click on the next button
6.  give a name for your VM. **Note: your VM name must be prefixed with
    your project name → *projectname*-*slave\_name***
7.  click on
    ![](/doc/img/Cloudstack_launch_vm.png "fig:/doc/img/Cloudstack_launch_vm.png")

You can see appear a notification in the upper-right corner. Clicking
on, you have a small window with the list of all current notifications
![](/doc/img/Cloudstack_notification.png "fig:/doc/img/Cloudstack_notification.png")

![](/doc/img/Cloudstack_show_instance.png "/doc/img/Cloudstack_show_instance.png")

This will create and start your VM. You will see the advancement of the
VM launching in the notification list. This VM will also appear as
*Created* in the Instance Tab of the CloudStack front-end. When the
creation is over and when the VM is launched, you can administrate it
(Stopping / Deleting / Starting) through the Instance Tab by clicking on
the various buttons. If you click, on your VM name on the tab, you get a
control panel of the VM in which you get more actions doable.
Especially, you will find the open console button
![](/doc/img/CI-CloudStack-OpenConsole.png "fig:/doc/img/CI-CloudStack-OpenConsole.png")
that opens a web terminal on the VM and lets you use the VM as a normal
machine.

![](/doc/img/Cloudstack_instance_commands.png "/doc/img/Cloudstack_instance_commands.png")

If you wish to use the newly created slave in Jenkins please read
[../jenkins\_tutorial\#providing-custom-build-slaves-using-the-cloudstack-portal](../jenkins_tutorial#providing-custom-build-slaves-using-the-cloudstack-portal "wikilink").

Creating a virtual machine from an ISO
--------------------------------------

To create a virtual machine from an ISO, you should first upload an ISO.
**This ISO should available from a http Web server** (https not yet
supported with our CloudStack version).

The formats supported by Cloudstack are :

- raw
- qcow2
- vhd
- vmdk

To do so, go in the template tab
![](/doc/img/Cloudstack_templates_button.png "fig:/doc/img/Cloudstack_templates_button.png")
of the CloudStack client, select ISO in the View field
![](/doc/img/Cloudstack_select_view.png "fig:/doc/img/Cloudstack_select_view.png")
and then click on
![](/doc/img/Cloudstack_register_iso.png "fig:/doc/img/Cloudstack_register_iso.png").
You should get the following screen:
![](/doc/img/Cloudstack_add_iso.png "fig:/doc/img/Cloudstack_add_iso.png")
You just have to fill this dialog, the required name is the one that
will appear in CloudStack so put an evident name for you to remember.
**Note that it must be prefixed with your project name →
*projectname*-*slave\_name***. Don't forget to fill the type of
operating system before clicking on **OK**. Your ISO will start getting
downloaded and you will be able to track its status in the ISO View
panel:
![](/doc/img/Cloudstack_view_iso.png "fig:/doc/img/Cloudstack_view_iso.png")

Now you can go through the step of creating a VM: first, go to the
Instances tab
![](/doc/img/Cloudstack_instances_button.png‎ "fig:/doc/img/Cloudstack_instances_button.png‎")
in the dashboard of the CloudStack platform and select the add instance
button
![](/doc/img/Cloudstack_add_instance_button.png "fig:/doc/img/Cloudstack_add_instance_button.png").

You should get the same 6 steps dialog than for creating a VM from a
template:

1.  ![Step 1: Choose from template or from ISO
    ](/doc/img/Cloudstack_add_instance_with_iso_1.png "fig:Step 1: Choose from template or from ISO ")
2.  ![Step 2: Select your
    template](/doc/img/Cloudstack_add_instance_with_iso_2.png "fig:Step 2: Select your template")
3.  ![Step 3: Select the amount of CPU and RAM you want for your
    VM](/doc/img/Cloudstack_add_instance_3.png "fig:Step 3: Select the amount of CPU and RAM you want for your VM")
4.  ![Step 4: Select extra
    disk](/doc/img/Cloudstack_add_instance_with_iso_4.png "fig:Step 4: Select extra disk")
5.  ![Step 5: Affinity configuration / ignore this step
    ](/doc/img/Cloudstack_add_instance_5.png "fig:Step 5: Affinity configuration / ignore this step ")
6.  ![Step 6: Network configuration / ignore this step
    ](/doc/img/Cloudstack_add_instance_6.png "fig:Step 6: Network configuration / ignore this step ")
7.  ![Step 7: Confirm the
    configuration](/doc/img/Cloudstack_add_instance_7.png "fig:Step 7: Confirm the configuration")

The only change with the way you create a VM from a Template are:

-   Choose ISO in the first step
-   Select the desired ISO in the second step
-   Select a disk size in the fourth step, you cannot choose no disk
    because no disk is provided with the ISO contrary to a template.

After that, your VM will be started with the ISO and you can do the
installation of the operating system using the console
![](/doc/img/CI-CloudStack-OpenConsole.png "fig:/doc/img/CI-CloudStack-OpenConsole.png").
Once the installation is finished, you can detach the ISO using the
"Detach ISO" button
![](/doc/img/CI-CloudStack-DetachISO.png "fig:/doc/img/CI-CloudStack-DetachISO.png")
in the VM control panel.

If you wish to use the newly created slave in Jenkins please read
[../jenkins\_tutorial\#providing-custom-build-slaves-using-the-cloudstack-portal](../jenkins_tutorial#providing-custom-build-slaves-using-the-cloudstack-portal "wikilink").

Saving your slave
-----------------

### A word about the template & snapshots backup policy

If your slave needs a subtancial amount of work to setup, it is advised
to create template for it, may it be public or private. It will then be
easy to replicate or re-create other slaves in minutes from the
template. If the "featured templates" provided fulfill your needs, or
if it require only a small effort to setup, then the template creation
might not be justified.

Only the templates and snapshots are automatically saved every day. The
slaves themselves are not, since they can be easily restored by the
templates (the featured templates, or your owns).

### Creating a template

To create a template, first prepare the VM you want to turn into a
template (installation, configuration, etc\...). Then stop it
![](/doc/img/CI-CloudStack-StopVM.png "fig:/doc/img/CI-CloudStack-StopVM.png")
in the instance view and confirm
![](/doc/img/Cloudstack_stop_instance.png "fig:/doc/img/Cloudstack_stop_instance.png").
When the VM is stopped
![](/doc/img/CI-CloudStack-VM_Stopped.png "fig:/doc/img/CI-CloudStack-VM_Stopped.png"),
then you can go to the storage tab
![](/doc/img/Cloudstack_storage_button.png "fig:/doc/img/Cloudstack_storage_button.png")
and select your VM drive, you will get the following screen:

![](/doc/img/Cloudstack_storage_view.png "/doc/img/Cloudstack_storage_view.png")

Click on the "Create Template" button
![](/doc/img/Cloudstack_template_create_button.png "fig:/doc/img/Cloudstack_template_create_button.png")
and you will get the template creation dialog:

![](/doc/img/Cloudstack_template_dialog.png "/doc/img/Cloudstack_template_dialog.png")

Select a name, a description and the corresponding operating system.
Select Public if you wish to make your template available in the
community tab of the template selection. Make it password enabled if you
have enabled the [password management by CloudStack in the
VM](../cloudstack_tutorialpasswordmanagement "wikilink").

Click on OK
and wait a long time. Then you should see your template in the Template
tab.

### Taking snapshot

**Bug:** This procedure is no more working after the last cloudstack
upgrade, sorry for the inconvenience. We are looking for a fix or a
workaround.

The data of a volume (a disk storage, or system disk) can be backed up
at a particular state by taking a snapshot of it.

-   The "Take Snapshot" button
    ![](/doc/img/CI-CloudStack-TakeSnapshotButton.png "fig:/doc/img/CI-CloudStack-TakeSnapshotButton.png")
    let you take a individual snapshot of the VM.
-   the "Setup recurring Shnapshot" button
    ![](/doc/img/CI-CloudStack-RecuringSnapshotButton.png "fig:/doc/img/CI-CloudStack-RecuringSnapshotButton.png")
    let you program automatic recurring snapshot.

Note that the first (manual) approach give you the possibility to stop
the virtual machine before taking its snapshot (recommended), while the
second one will take the snapshot while the virtual machine is running,
which may not be as robust.

![](/doc/img/Cloudstack_reccuring_snapshot.png "/doc/img/Cloudstack_reccuring_snapshot.png")

A snapshot can then be restored in two ways:

-   Create a volume from it.
-   Create a template from it.

Add a storage disk to an existing slave
---------------------------------------

It is possible to create an new storage disk, and add to an existing
virtual machine.

1.  Create a new storage disk clicking on
    ![](/doc/img/Cloudstack_button_add_volume.png "fig:/doc/img/Cloudstack_button_add_volume.png")
2.  Attach the new disk to the virtual machine instance clicking on
    ![](/doc/img/Cloudstack_trombone_plus.png "fig:/doc/img/Cloudstack_trombone_plus.png")
3.  Start and stop the virtual machine instance, so that the new disk is
    detected. (restart is not enough).
4.  Then it's up to the user to make the new disk being mounted
    automatically. On GNU/Linux, the device path is **/dev/vdb** or
    another path finded

by **lsblk \--fs**

`  (you can follow `[`this`` ``tutorial`](https://help.ubuntu.com/community/InstallingANewHardDrive)` for instance to format and mount it - Do not use GParted but the command line instead)`

1.  ![Step 1: Create new storage
    disk.](/doc/img/Cloudstack_add_volume_1.png "fig:Step 1: Create new storage disk.")
2.  ![Step 2: Attach the disk to the virtual
    machine.](/doc/img/Cloudstack_new_disk.png "fig:Step 2: Attach the disk to the virtual machine.")

### Formatting and using a disk for a MacOSX slave

Prerequisites: creating and attaching a disk with CI portal (extra disk space)
or by CloudStack portal as explained above.

1. Find the disk device with `diskutil list`, by example `/dev/disk0` ;
2. Format and mount the disk on `/Volumes/builds`:
```bash
diskutil eraseDisk JHFS+ builds /dev/disk0
```
3. Copy the initial `/builds` on `/Volumes/builds/`:
```bash
rsync -a /builds/ /Volumes/builds
```
4. Mount automatically the new disk partition  `/dev/disk0s2` on `/builds`,
you need to know `Volume UUID` to add e line in `/etc/fstab`:

```bash
diskutil umountDisk /dev/disk0
diskutil info disk0s2 | grep 'Volume UUID'
   Volume UUID:              <YOUR-VOLUME-UUID>
sudo EDITOR=nano vifs
# Add the line below at the end of fstab:
UUID=<YOUR-VOLUME-UUID> /builds hfs rw
# You need to reboot because mount -a doesn't work
sudo reboot
```

You can use this procedure for Unix slaves replacing `diskutil` by equivalent in your slave system.  

Next Step
---------

Once your slaves are correctly instantiated and tuned, you can proceed
to the next step:

[→ Slaves Access Tutorial](../slaves_access_tutorial "wikilink")

This part will teach you how to open a remote session on your virtual
machines. This will allow you to handle the administration tasks (change
the passwords, install new packages, \...). Linux slaves are accessed
via a SSH session and Windows slaves via a Remote Desktop session.
